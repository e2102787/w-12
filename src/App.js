import businesscard from './businesscard.jpg';
import './App.css';

function App() {
  return (
    <div className="App">

      <div className="image">
      <img src={businesscard} alt="businesscard"></img>
      </div>
      <div className="businesscard_info">
        <div className="header">
      <span>
          Niina Kettunen
      </span>
      </div>

      <span>
          Opiskelija
        </span>

        <span>
        Vaasan AMK
        </span>

        <span>
          e210787@edu.vamk.fi
        </span>

        <span>
          040 1234 567
        </span>

        <span>
        Wolffintie 30, FI-65200 VAASA, Finland
        </span>
        </div>
    </div>
  );
}

export default App;
